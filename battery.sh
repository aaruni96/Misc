#!/bin/bash

PATH_TO_BATTERY='/org/freedesktop/UPower/devices/battery_BAT0'
MQTT_HOST=
MQTT_TOPIC='systemx/batt/stat'
MQTT_USER=
MQTT_PASSWORD=
perc=$(upower -i $PATH_TO_BATTERY | grep 'perc' | cut -c 21-30 | sed 's/\([0-9][0-9]*\).*/\1/')

mosquitto_pub -h $MQTT_HOST -t $MQTT_TOPIC -m "$perc" -r -u $MQTT_USER -P $MQTT_PASSWORD



#connfig.yaml for Home Assistant :

#sensor:
#  - platform: mqtt
#    name: "System X Battery Percentage"
#    state_topic: "systemx/batt/stat"
#    unit_of_measurement: "%"